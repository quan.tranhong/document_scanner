import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'document_scanner_result.dart';
import 'edge_detection_shape/edge_detection_shape.dart';

class EdgeDetectionPreview extends StatefulWidget {
  EdgeDetectionPreview({
    required this.imagePath,
    required this.edgeDetectionResult,
    required this.onChangedPosition,
    this.androidOriginalWidth,
    this.androidOriginalHeight,
  });

  final String imagePath;
  final double? androidOriginalWidth;
  final double? androidOriginalHeight;
  final DocumentScannerResult edgeDetectionResult;
  final Function(DocumentScannerResult?) onChangedPosition;

  @override
  _EdgeDetectionPreviewState createState() => _EdgeDetectionPreviewState();
}

class _EdgeDetectionPreviewState extends State<EdgeDetectionPreview> {
  GlobalKey imageWidgetKey = GlobalKey();

  @override
  Widget build(BuildContext mainContext) {
    return Center(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Center(child: Text('Loading ...')),
          Image.file(File(widget.imagePath),
              fit: BoxFit.contain, key: imageWidgetKey),
          FutureBuilder<ui.Image>(
              future: loadUiImage(widget.imagePath),
              builder:
                  (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
                return _getEdgePaint(snapshot, context);
              }),
        ],
      ),
    );
  }

  Widget _getEdgePaint(
      AsyncSnapshot<ui.Image> imageSnapshot, BuildContext context) {
    if (imageSnapshot.connectionState == ConnectionState.waiting)
      return Container();

    if (imageSnapshot.hasError) return Text('Error: ${imageSnapshot.error}');

    if (widget.edgeDetectionResult == null) return Container();

    final keyContext = imageWidgetKey.currentContext;

    if (keyContext == null) {
      return Container();
    }

    final box = keyContext.findRenderObject() as RenderBox;

    List<Offset> points = [
      widget.edgeDetectionResult.topLeft,
      widget.edgeDetectionResult.topRight,
      widget.edgeDetectionResult.bottomRight,
      widget.edgeDetectionResult.bottomLeft,
    ];
    print('quanth: crop: androidOriginalWidth= ${widget.androidOriginalWidth ?? 0}, androidOriginalHeight= ${widget.androidOriginalHeight ?? 0}');
    return EdgeDetectionShape(
      originalImageSize: Size(
          (Platform.isIOS)
              ? (imageSnapshot.data?.width.toDouble() ?? 0)
              : (widget.androidOriginalWidth ?? 0),
          (Platform.isIOS)
              ? (imageSnapshot.data?.height.toDouble() ?? 0)
              : (widget.androidOriginalHeight ?? 0)),
      renderedImageSize: Size(box.size.width, box.size.height),
      edgeDetectionResult: widget.edgeDetectionResult,
      onChangedPosition: widget.onChangedPosition,
    );
    // return Container(
    //   width: 150,
    //   height: 150,
    //   color: Colors.red.withOpacity(0.5),
    // );

    /*
      return CustomPaint(
        size: Size(imageSnapshot.data?.width.toDouble() ?? 0, imageSnapshot.data?.height.toDouble() ?? 0),
        painter: EdgePainter(
            points: points,
            color: Colors.red));
     */
  }

  Future<ui.Image> loadUiImage(String imageAssetPath) async {
    final Uint8List data = await File(imageAssetPath).readAsBytes();
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(Uint8List.view(data.buffer), (ui.Image image) {
      return completer.complete(image);
    });
    return completer.future;
  }
}
