import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

import '../document_scanner_result.dart';
import 'edge_painter.dart';
import 'magnifier.dart';
import 'touch_bubble.dart';

class EdgeDetectionShape extends StatefulWidget {
  EdgeDetectionShape({
    required this.renderedImageSize,
    required this.originalImageSize,
    required this.edgeDetectionResult,
    required this.onChangedPosition,
  });

  final Size renderedImageSize;
  final Size originalImageSize;
  final DocumentScannerResult? edgeDetectionResult;
  final Function(DocumentScannerResult?) onChangedPosition;

  @override
  _EdgeDetectionShapeState createState() => _EdgeDetectionShapeState();
}

class _EdgeDetectionShapeState extends State<EdgeDetectionShape> {
  late double edgeDraggerSize;

  DocumentScannerResult? edgeDetectionResult;
  List<Offset>? points;

  double? renderedImageWidth;
  double? renderedImageHeight;
  double? top;
  double? left;

  Offset? currentDragPosition;

  @override
  void didChangeDependencies() {
    double shortestSide = min(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    edgeDraggerSize = shortestSide / 12;
    super.didChangeDependencies();
  }

  @override
  void initState() {
    edgeDetectionResult = widget.edgeDetectionResult;
    _calculateDimensionValues();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Magnifier(
      visible: currentDragPosition != null,
      position: currentDragPosition ?? Offset(0,0),
      child: Stack(
        children: [
          _getTouchBubbles(),
          CustomPaint(
            painter: EdgePainter(
              points: points ?? [],
              color: Colors.white
            )
          )
        ],
      )
    );
  }

  void _calculateDimensionValues() {
    top = 0.0;
    left = 0.0;

    double widthFactor = widget.renderedImageSize.width / widget.originalImageSize.width;
    double heightFactor = widget.renderedImageSize.height / widget.originalImageSize.height;
    double sizeFactor = min(widthFactor, heightFactor);

    renderedImageHeight = widget.originalImageSize.height * sizeFactor;
    top = ((widget.renderedImageSize.height - (renderedImageHeight ?? 0)) / 2);

    renderedImageWidth = widget.originalImageSize.width * sizeFactor;
    left = ((widget.renderedImageSize.width - (renderedImageWidth ?? 0)) / 2);
  }

  Offset _getNewPositionAfterDrag(Offset position, double renderedImageWidth, double renderedImageHeight) {
    return Offset(
      position.dx / renderedImageWidth,
      position.dy / renderedImageHeight
    );
  }

  Offset _clampOffset(Offset givenOffset) {
    double absoluteX = givenOffset.dx * (renderedImageWidth ?? 0);
    double absoluteY = givenOffset.dy * (renderedImageHeight ?? 0);

    return Offset(
      absoluteX.clamp(0.0, (renderedImageWidth ?? 0)) / (renderedImageWidth ?? 1),
      absoluteY.clamp(0.0, (renderedImageHeight ?? 0)) / (renderedImageHeight ?? 1)
    );
  }

  Widget _getTouchBubbles() {
    double widthFactor = widget.renderedImageSize.width / widget.originalImageSize.width;
    double heightFactor = widget.renderedImageSize.height / widget.originalImageSize.height;
    double sizeFactor = min(widthFactor, heightFactor);
    points = [
      (Platform.isIOS || Platform.isAndroid) ? Offset(
        (left ?? 0) + (edgeDetectionResult?.topLeft.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.topLeft.dy ?? 0) * sizeFactor
      ) : Offset(
        /*(left ?? 0) + (edgeDetectionResult?.topLeft.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.topLeft.dy ?? 0) * sizeFactor*/
          (left ?? 0) + (edgeDetectionResult?.topLeft.dx ?? 0) * (renderedImageWidth ?? 0),
          (top ?? 0) + (edgeDetectionResult?.topLeft.dy ?? 0) * (renderedImageHeight ?? 0)
      ),
      (Platform.isIOS || Platform.isAndroid) ? Offset(
          (left ?? 0) + (edgeDetectionResult?.topRight.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.topRight.dy ?? 0) * sizeFactor
      ) : Offset(
        (left ?? 0) + (edgeDetectionResult?.topRight.dx ?? 0) * (renderedImageWidth ?? 0),
        (top ?? 0) + (edgeDetectionResult?.topRight.dy ?? 0) * (renderedImageHeight ?? 0)
      ),
      (Platform.isIOS || Platform.isAndroid) ? Offset(
          (left ?? 0) + (edgeDetectionResult?.bottomRight.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.bottomRight.dy ?? 0) * sizeFactor
      ) : Offset(
        /*(left ?? 0) + (edgeDetectionResult?.topLeft.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.topLeft.dy ?? 0) * sizeFactor*/
        (left ?? 0) + (edgeDetectionResult?.bottomRight.dx ?? 0) * (renderedImageWidth ?? 0),
        (top ?? 0) + ((edgeDetectionResult?.bottomRight.dy ?? 0) * (renderedImageHeight ?? 0))
      ),
      (Platform.isIOS || Platform.isAndroid) ? Offset(
          (left ?? 0) + (edgeDetectionResult?.bottomLeft.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.bottomLeft.dy ?? 0) * sizeFactor
      ) : Offset(
          /*(left ?? 0) + (edgeDetectionResult?.bottomLeft.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.bottomLeft.dy ?? 0) * sizeFactor*/
        (left ?? 0) + (edgeDetectionResult?.bottomLeft.dx ?? 0) * (renderedImageWidth ?? 0),
        (top ?? 0) + (edgeDetectionResult?.bottomLeft.dy ?? 0) * (renderedImageHeight ?? 0)
      ),
      (Platform.isIOS || Platform.isAndroid) ? Offset(
          (left ?? 0) + (edgeDetectionResult?.topLeft.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.topLeft.dy ?? 0) * sizeFactor
      ) : Offset(
        /*(left ?? 0) + (edgeDetectionResult?.topLeft.dx ?? 0) * sizeFactor,
          (top ?? 0) + (edgeDetectionResult?.topLeft.dy ?? 0) * sizeFactor*/
          (left ?? 0) + (edgeDetectionResult?.topLeft.dx ?? 0) * (renderedImageWidth ?? 0),
          (top ?? 0) + (edgeDetectionResult?.topLeft.dy ?? 0) * (renderedImageHeight ?? 0)
      ),
    ];

    final Function onDragFinished = () {
      currentDragPosition = null;
      setState(() {});
    };

    return Container(
      width: widget.originalImageSize.width,
      height: widget.originalImageSize.height,
      child: Stack(
        children: [
          Positioned(
            child: TouchBubble(
              size: edgeDraggerSize,
              onDrag: (position) {
                setState(() {
                  if (Platform.isIOS || Platform.isAndroid){
                    double x = edgeDetectionResult?.topLeft.dx ?? 0;
                    double y = edgeDetectionResult?.topLeft.dy ?? 0;
                    edgeDetectionResult?.topLeft = Offset(x + position.dx, y + position.dy);
                  } else {
                    currentDragPosition = Offset((points?[0].dx ?? 0), (points?[0].dy ?? 0));
                    Offset newTopLeft = _getNewPositionAfterDrag(
                        position, (renderedImageWidth ?? 0), (renderedImageHeight ?? 0)
                    );
                    edgeDetectionResult?.topLeft = _clampOffset(
                      (edgeDetectionResult?.topLeft ?? Offset(0, 0)) + newTopLeft
                    );
                  }

                  currentDragPosition = Offset((points?[0].dx ?? 0), (points?[0].dy ?? 0));
                  widget.onChangedPosition(edgeDetectionResult);
                });
              },
              onDragFinished: onDragFinished
            ),
            left: (points?[0].dx ?? 0) - (edgeDraggerSize / 2),
            top: (points?[0].dy ?? 0) - (edgeDraggerSize / 2)
          ),
          Positioned(
            child: TouchBubble(
              size: edgeDraggerSize,
              onDrag: (position) {
                setState(() {
                  if (Platform.isIOS || Platform.isAndroid){
                    double x = edgeDetectionResult?.topRight.dx ?? 0;
                    double y = edgeDetectionResult?.topRight.dy ?? 0;
                    edgeDetectionResult?.topRight = Offset(x + position.dx, y + position.dy);
                    currentDragPosition = Offset((points?[1].dx ?? 0), (points?[1].dy ?? 0));
                    widget.onChangedPosition(edgeDetectionResult);
                  } else {
                    Offset newTopRight = _getNewPositionAfterDrag(
                      position, (renderedImageWidth ?? 0), (renderedImageHeight ?? 0)
                    );
                    edgeDetectionResult?.topRight = _clampOffset(
                      (edgeDetectionResult?.topRight ?? Offset(0, 0)) + newTopRight
                    );
                    currentDragPosition = Offset((points?[1].dx ?? 0), (points?[1].dy ?? 0));
                  }
                });
              },
              onDragFinished: onDragFinished
            ),
            left: (points?[1].dx ?? 0) - (edgeDraggerSize / 2),
            top: (points?[1].dy ?? 0) - (edgeDraggerSize / 2)
          ),
          Positioned(
            child: TouchBubble(
                size: edgeDraggerSize,
                onDrag: (position) {
                  setState(() {
                    if (Platform.isIOS || Platform.isAndroid){
                      double x = edgeDetectionResult?.bottomRight.dx ?? 0;
                      double y = edgeDetectionResult?.bottomRight.dy ?? 0;
                      edgeDetectionResult?.bottomRight = Offset(x + position.dx, y + position.dy);
                      currentDragPosition = Offset((points?[2].dx ?? 0), (points?[2].dy ?? 0));
                      widget.onChangedPosition(edgeDetectionResult);
                    } else {
                      Offset newBottomRight = _getNewPositionAfterDrag(
                        position, (renderedImageWidth ?? 0), (renderedImageHeight ?? 0)
                      );
                      edgeDetectionResult?.bottomRight = _clampOffset(
                        (edgeDetectionResult?.bottomRight ?? Offset(0, 0)) + newBottomRight
                      );
                      currentDragPosition = Offset((points?[2].dx ?? 0), (points?[2].dy ?? 0));
                    }
                  });
                },
                onDragFinished: onDragFinished
            ),
            left: (points?[2].dx ?? 0) - (edgeDraggerSize / 2),
            top: (points?[2].dy ?? 0) - (edgeDraggerSize / 2)
          ),
          Positioned(
            child: TouchBubble(
                size: edgeDraggerSize,
                onDrag: (position) {
                  setState(() {
                    if (Platform.isIOS || Platform.isAndroid){
                      double x = edgeDetectionResult?.bottomLeft.dx ?? 0;
                      double y = edgeDetectionResult?.bottomLeft.dy ?? 0;
                      edgeDetectionResult?.bottomLeft = Offset(x + position.dx, y + position.dy);
                      currentDragPosition = Offset((points?[3].dx ?? 0), (points?[3].dy ?? 0));
                      widget.onChangedPosition(edgeDetectionResult);
                    } else {
                      Offset newBottomLeft = _getNewPositionAfterDrag(
                          position, (renderedImageWidth ?? 0), (renderedImageHeight ?? 0)
                      );
                      edgeDetectionResult?.bottomLeft = _clampOffset(
                          (edgeDetectionResult?.bottomLeft ?? Offset(0, 0)) + newBottomLeft
                      );
                      currentDragPosition = Offset((points?[3].dx ?? 0), (points?[3].dy ?? 0));
                    }
                  });
                },
                onDragFinished: onDragFinished
            ),
            left: (points?[3].dx ?? 0) - (edgeDraggerSize / 2),
            top: (points?[3].dy ?? 0) - (edgeDraggerSize / 2)
          ),
        ],
      ),
    );
  }
}