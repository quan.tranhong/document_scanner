import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:document_scanner/document_scanner.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'edge_detection/document_scanner_result.dart';
import 'edge_detection/edge_detection_preview.dart';

const String _methodChannelIdentifier = 'document_scanner';
class DocumentScanAndCrop extends StatefulWidget {
  final Function(ScannedImage, Map<String, dynamic>) onDocumentScanned;
  final Function(DocumentScannerResult?) onChangedPosition;
  final Function onCroppedImage;

  DocumentScanAndCrop({
    required this.onDocumentScanned,
    required this.onChangedPosition,
    required this.onCroppedImage,
  });

  final MethodChannel _channel = const MethodChannel(_methodChannelIdentifier);

  @override
  State<DocumentScanAndCrop> createState() => _DocumentScanAndCropState();
}

class _DocumentScanAndCropState extends State<DocumentScanAndCrop> {
  File? scannedDocument;
  ScannedImage? _scannedImage;
  Future<PermissionStatus>? cameraPermissionFuture;
  DocumentScannerResult? edgeDetectionResult;
  double? androidOriginalWidth;
  double? androidOriginalHeight;

  @override
  void initState() {
    cameraPermissionFuture = Permission.camera.request();
    // widget._channel.setMethodCallHandler(_onDocumentScanned);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<PermissionStatus>(
        future: cameraPermissionFuture,
        builder:
            (BuildContext context, AsyncSnapshot<PermissionStatus> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data?.isGranted ?? false)
              return Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Expanded(
                        child: edgeDetectionResult != null
                                ? EdgeDetectionPreview(
                                    imagePath: scannedDocument?.path ?? "",
                                    edgeDetectionResult: edgeDetectionResult ??
                                        DocumentScannerResult(
                                          topLeft: Offset(0, 0),
                                          topRight: Offset(0, 0),
                                          bottomLeft: Offset(0, 0),
                                          bottomRight: Offset(0, 0),
                                        ),
                                    onChangedPosition: widget.onChangedPosition,
                                    androidOriginalWidth: androidOriginalWidth,
                                    androidOriginalHeight: androidOriginalHeight,
                                  )
                                : scannedDocument != null
                                    ? Image(
                                        image: FileImage(scannedDocument!),
                                      )
                                    : DocumentScanner(
                                        channel: widget._channel,
                                        // documentAnimation: false,
                                        noGrayScale: true,
                                        onDocumentScanned:
                                            (ScannedImage scannedImage,
                                                Map<String, dynamic> argsAsMap) {
                                          widget.onDocumentScanned(scannedImage, argsAsMap);
                                          print("document : " +
                                              scannedImage.croppedImage!);

                                          setState(() {
                                            _scannedImage = scannedImage;
                                            scannedDocument = _scannedImage?.getScannedDocumentAsFile();
                                            if(argsAsMap["width"] !=
                                                null && argsAsMap["height"] !=
                                                null){
                                              androidOriginalWidth = argsAsMap["width"].toDouble();
                                              androidOriginalHeight = argsAsMap["height"].toDouble();
                                            }

                                            if (argsAsMap["rectangleCoordinates"] !=
                                                null) {
                                              Offset topLeft = Offset(
                                                argsAsMap["rectangleCoordinates"]
                                                    ["topLeft"]["x"],
                                                argsAsMap["rectangleCoordinates"]
                                                    ["topLeft"]["y"],
                                              );
                                              Offset topRight = Offset(
                                                argsAsMap["rectangleCoordinates"]
                                                    ["topRight"]["x"],
                                                argsAsMap["rectangleCoordinates"]
                                                    ["topRight"]["y"],
                                              );
                                              Offset bottomLeft = Offset(
                                                argsAsMap["rectangleCoordinates"]
                                                    ["bottomLeft"]["x"],
                                                argsAsMap["rectangleCoordinates"]
                                                    ["bottomLeft"]["y"],
                                              );
                                              Offset bottomRight = Offset(
                                                argsAsMap["rectangleCoordinates"]
                                                    ["bottomRight"]["x"],
                                                argsAsMap["rectangleCoordinates"]
                                                    ["bottomRight"]["y"],
                                              );
                                              edgeDetectionResult =
                                                  DocumentScannerResult(
                                                topLeft: topLeft,
                                                topRight: topRight,
                                                bottomLeft: bottomLeft,
                                                bottomRight: bottomRight,
                                              );
                                            }
                                            // imageLocation = image;
                                          });
                                        },
                                      ),
                      ),
                    ],
                  ),
                  /*scannedDocument != null
                      ? Positioned(
                          bottom: 20,
                          left: 0,
                          right: 0,
                          child: RaisedButton(
                              child: Text("retry"),
                              onPressed: () {
                                setState(() {
                                  scannedDocument = null;
                                });
                              }),
                        )
                      : Container(),*/
                  scannedDocument != null
                      ? Positioned(
                          bottom: 50,
                          left: 0,
                          right: 0,
                          child: GestureDetector(
                            onTap: () {
                              widget.onCroppedImage();
                            },
                            child: Container(
                              width: 80,
                              height: 80,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.blue),
                              child: Icon(Icons.crop,
                                  size: 45, color: Colors.white),
                            ),
                          ),
                        )
                      : Positioned(
                          bottom: 50,
                          left: 0,
                          right: 0,
                          child: GestureDetector(
                            onTap: () {
                              widget._channel.invokeMethod('onCaptureImage');
                            },
                            child: Container(
                              width: 80,
                              height: 80,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.blue),
                              child: Icon(Icons.camera,
                                  size: 45, color: Colors.white),
                            ),
                          ),
                        ),
                ],
              );
            else
              return Center(
                child: Text("camera permission denied"),
              );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Future<dynamic> _onDocumentScanned(MethodCall call) async {
    try {
      if (call.method == "onPictureTaken") {
        Map<String, dynamic> argsAsMap =
        Map<String, dynamic>.from(call.arguments);

        ScannedImage scannedImage = ScannedImage.fromMap(argsAsMap);

        // ScannedImage scannedImage = ScannedImage(
        //     croppedImage: argsAsMap["croppedImage"],
        //     initialImage: argsAsMap["initialImage"]);

        // print("scanned image decoded");
        // print(scannedImage.toJson());

        if (scannedImage.croppedImage != null) {
          // print("scanned image not null");
          widget.onDocumentScanned(scannedImage, argsAsMap);
        }
      } else if (call.method == "onCropImage") {}
    } catch (e) {
      print("${e.toString()}");
    }
    if (call.method == "onCropImage") {
      Map<String, dynamic> argsAsMap =
          Map<String, dynamic>.from(call.arguments);
      print("quanth: callback from native crop image");
      doCrop(
        topLeft: edgeDetectionResult?.topLeft,
        topRight: edgeDetectionResult?.topRight,
        bottomLeft: edgeDetectionResult?.bottomLeft,
        bottomRight: edgeDetectionResult?.bottomRight,
      );
    }

    return;
  }

  void doCrop({
    Offset? topLeft,
    Offset? topRight,
    Offset? bottomLeft,
    Offset? bottomRight,
  }) {
    String message =
        'quanth: crop \n topLeft dx= ${topLeft?.dx ?? 0}, dy= ${topLeft?.dy ?? 0}\n '
        'topRight dx= ${topRight?.dx ?? 0}, dy= ${topRight?.dy ?? 0}\n'
        'bottomLeft dx= ${bottomLeft?.dx ?? 0}, dy= ${bottomLeft?.dy ?? 0}\n'
        'bottomRight dx= ${bottomRight?.dx ?? 0}, dy= ${bottomRight?.dy ?? 0}\n';
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      backgroundColor: Colors.green,
      textColor: Colors.white,
    );
    print(
        'quanth: crop \n topLeft dx= ${topLeft?.dx ?? 0}, dy= ${topLeft?.dy ?? 0}\n '
        'topRight dx= ${topRight?.dx ?? 0}, dy= ${topRight?.dy ?? 0}\n'
        'bottomLeft dx= ${bottomLeft?.dx ?? 0}, dy= ${bottomLeft?.dy ?? 0}\n'
        'bottomRight dx= ${bottomRight?.dx ?? 0}, dy= ${bottomRight?.dy ?? 0}\n');
  }
}
