#import "DocumentScannerView.h"
#import "IPDFCameraViewController.h"

@implementation DocumentScannerView
- (instancetype)init{
    self = [super init];
        self.detectionRefreshRateInMS = 10;
        self.overlayColor = [UIColor colorWithRed: 0.00 green: 0.00 blue: 1.00 alpha: 0.50];
        self.enableTorch = false;
        self.useFrontCam = false;
        self.useBase64 = false;
        self.saveInAppDocument = true;
        self.captureMultiple = false;
        self.detectionCountBeforeCapture = 8;
        self.detectionRefreshRateInMS = 10;
        self.saturation = 1;
        self.quality = 1;
        self.brightness = 0.2;
        self.contrast = 1.4;
        self.durationBetweenCaptures = 0;
    if (self) {
        [self setEnableBorderDetection:YES];
        [self setDelegate: self];
    }

    return self;
}

-(instancetype) initWithChannel:(FlutterMethodChannel *)channel {
      DocumentScannerView* instance = [DocumentScannerView new];
      instance.flutterChannel = channel;

    
      return instance;
}
//-(instancetype)initWithChannelAndArgs:(FlutterMethodChannel *)channel brightness:(float)channelBrightness contrast:(float)channelContrast {
//    DocumentScannerView* instance = [[DocumentScannerView alloc] init:channelBrightness contrast:channelContrast];
//    instance.flutterChannel = channel;
//
//
//    return instance;
//}

-(void) onPictureTaken {
    printf("on picture taken");
}

-(void) onRectangleDetect {
    printf("on rect detect");
}

- (void) didDetectRectangle:(CIRectangleFeature *)rectangle withType:(IPDFRectangeType)type {
    switch (type) {
        case IPDFRectangeTypeGood:
            self.stableCounter ++;
            break;
        default:
            self.stableCounter = 0;
            break;
    }
//    if (self.onRectangleDetect) {
//        self.onRectangleDetect(@{@"stableCounter": @(self.stableCounter), @"lastDetectionType": @(type)});
//    }
//
    if (self.stableCounter > self.detectionCountBeforeCapture &&
        [NSDate timeIntervalSinceReferenceDate] > self.lastCaptureTime + self.durationBetweenCaptures) {
        self.lastCaptureTime = [NSDate timeIntervalSinceReferenceDate];
        self.stableCounter = 0;
        [self capture];
    }
}
- (void) onPictureTaken: (NSDictionary*) result {
    printf("on picture taken");
}

- (void) onCropImage: (NSString*) imageUrl
    :(double) topLeftX
    :(double) topLeftY
    :(double) topRightX
    :(double) topRightY
    :(double) bottomLeftX
    :(double) bottomLeftY
    :(double) bottomRightX
    :(double) bottomRightY {
    NSLog(@"quan: IPDF callback onCropImage %@", imageUrl);
    NSLog(@"quan: IPDF callback onCropImage topLeftX= %.20f", topLeftX);
    NSLog(@"quan: IPDF callback onCropImage topLeftY= %.20f", topLeftY);
    NSLog(@"quan: IPDF callback onCropImage topRightX= %.20f", topRightX);
    NSLog(@"quan: IPDF callback onCropImage topRightY= %.20f", topRightY);
    NSLog(@"quan: IPDF callback onCropImage bottomLeftX= %.20f", bottomLeftX);
    NSLog(@"quan: IPDF callback onCropImage bottomLeftY= %.20f", bottomLeftY);
    NSLog(@"quan: IPDF callback onCropImage bottomRightX= %.20f", bottomRightX);
    NSLog(@"quan: IPDF callback onCropImage bottomRightY= %.20f", bottomRightY);

    NSLog(@"quan: IPDF callback onCropImage done!");
    [self->_flutterChannel invokeMethod:@"onCropImage" arguments:@{
        @"croppedImage": @"croppedImage",
        @"initialImage": @"initialImage",
        @"rectangleCoordinates": @"rectangleCoordinates"
    }];

    //dispatch_async(dispatch_get_main_queue(), ^{
    //    NSLog(@"quan: IPDF callback onCropImage");
    //    [self->_flutterChannel invokeMethod:@"onCropImage" arguments:@{
    //         @"croppedImage": @"croppedImage",
    //         @"initialImage": @"initialImage",
    //         @"rectangleCoordinates": @"rectangleCoordinates"
    //    }];
    //});
}

- (void) onCaptureImage {
    [self capture];
    // do nothing
}

- (void) capture {
    
    [self captureImageWithCompletionHander:^(UIImage *croppedImage, UIImage *initialImage, CIRectangleFeature *rectangleFeature) {
//      if (self.onPictureTaken) {
            NSData *croppedImageData = UIImageJPEGRepresentation(croppedImage, self.quality);

            if (initialImage.imageOrientation != UIImageOrientationUp) {
                UIGraphicsBeginImageContextWithOptions(initialImage.size, false, initialImage.scale);
                [initialImage drawInRect:CGRectMake(0, 0, initialImage.size.width
                                                    , initialImage.size.height)];
                initialImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }
            NSData *initialImageData = UIImageJPEGRepresentation(initialImage, self.quality);

            /*
             RectangleCoordinates expects a rectangle viewed from portrait,
             while rectangleFeature returns a rectangle viewed from landscape, which explains the nonsense of the mapping below.
             Sorry about that.
             */
            id rectangleCoordinates = rectangleFeature ? @{
                                     @"topLeft": @{ @"y": @(rectangleFeature.bottomLeft.x), @"x": @(rectangleFeature.bottomLeft.y)},
                                     @"topRight": @{ @"y": @(rectangleFeature.topLeft.x), @"x": @(rectangleFeature.topLeft.y)},
                                     @"bottomLeft": @{ @"y": @(rectangleFeature.bottomRight.x), @"x": @(rectangleFeature.bottomRight.y)},
                                     @"bottomRight": @{ @"y": @(rectangleFeature.topRight.x), @"x": @(rectangleFeature.topRight.y)},
                                     } : [NSNull null];
            if (self.useBase64) {
                
               dispatch_async(dispatch_get_main_queue(), ^{
                       printf("calling flutter onPictureTaken base64");
                       [self->_flutterChannel invokeMethod:@"onPictureTaken" arguments:@{
                            @"croppedImage": [croppedImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength],
                            @"initialImage": [initialImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength],
                            @"rectangleCoordinates": rectangleCoordinates }];
               });
          
//              onPictureTaken(@{
//                                    @"croppedImage": [croppedImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength],
//                                    @"initialImage": [initialImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength],
//                                    @"rectangleCoordinates": rectangleCoordinates });
            }
            else {
                NSString *dir = NSTemporaryDirectory();
                if (self.saveInAppDocument) {
                    dir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                }
               NSString *croppedFilePath = [dir stringByAppendingPathComponent:[NSString stringWithFormat:@"cropped_img_%i.jpeg",(int)[NSDate date].timeIntervalSince1970]];
               NSString *initialFilePath = [dir stringByAppendingPathComponent:[NSString stringWithFormat:@"initial_img_%i.jpeg",(int)[NSDate date].timeIntervalSince1970]];

              [croppedImageData writeToFile:croppedFilePath atomically:YES];
              [initialImageData writeToFile:initialFilePath atomically:YES];
                
           
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                        printf("calling flutter onPictureTaken file");
//                    NSLog(croppedFilePath);
//                    NSLog(initialFilePath);
//                    NSLog(rectangleCoordinates);
                   
                    [self->_flutterChannel invokeMethod:@"onPictureTaken" arguments:@{
                        @"croppedImage": croppedFilePath,
                        @"initialImage": initialFilePath,
                        @"rectangleCoordinates": rectangleCoordinates
                    }];
                    
                });


//                self.onPictureTaken(@{
//                                     @"croppedImage": croppedFilePath,
//                                     @"initialImage": initialFilePath,
//                                     @"rectangleCoordinates": rectangleCoordinates });
            }
//        }

        if (!self.captureMultiple) {
          [self stop];
        }
    }];

}


@end
